import React from "react";
import ReactDOM from "react-dom/client";

// JSX
const Title = () => {
  return (
    <h1 className="head" tabIndex="1">
      Namaste React using JSX Element
    </h1>
  );
};

const data = 1000;

const HeadingComponent = () => {
  return (
    <div id="container">
      <Title />
      <Title></Title>
      <h1 className="heading">Namaste React using functional component</h1>
    </div>
  );
};

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(<HeadingComponent />);
