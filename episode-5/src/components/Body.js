import RestaurantCard from "./RestaurantCard";
import { restaurantsData } from "../utils/mockData";
import { useState } from "react";

const Body = () => {
  const [listOfRestaurant, setListofRestaurant] = useState([
    ...restaurantsData,
  ]);

  function filteredRestaurant(e) {
    const filterEntries = listOfRestaurant.filter((restaurant) => {
      return restaurant.info.avgRating > 4;
    });
    setListofRestaurant(filterEntries);
  }

  return (
    <div className="body">
      <div className="filter">
        <button className="filter-btn" onClick={(e) => filteredRestaurant(e)}>
          Top Rated Restaurants
        </button>
      </div>
      <div className="res-container">
        {listOfRestaurant.map((resObj) => {
          return <RestaurantCard key={resObj.info.id} resData={resObj} />;
        })}
      </div>
    </div>
  );
};

export default Body;
