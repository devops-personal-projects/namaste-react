import { useState } from "react";
import { CDN_URL } from "../utils/constants";
import { Link } from "react-router-dom";
import useOnlineStatus from "../utils/useOnlineStatus";

const Header = () => {
  const [loggedIn, setLoggedIn] = useState("Login");

  const onlineStatus = useOnlineStatus();

  function changeLogin(e) {
    loggedIn === "Login" ? setLoggedIn("Logout") : setLoggedIn("Login");
  }

  return (
    <div className="flex justify-between bg-pink-100 shadow-lg sm:bg-yellow-50 lg:bg-green-50">
      <div className="logo-container">
        <img src={CDN_URL} className="w-56" />
      </div>
      <div className="flex items-center">
        <ul className="flex p-4 m-4">
          <li className="px-4">
            Online Status: {onlineStatus === true ? "✅" : "❎"}
          </li>
          <li className="px-4">
            <Link to="/">Home</Link>
          </li>
          <li className="px-4">
            <Link to="/about">About Us</Link>
          </li>
          <li className="px-4">
            <Link to="/contact">Contact Us</Link>
          </li>
          <li className="px-4">
            <Link to="/grocery">Grocery</Link>
          </li>
          <li className="px-4">Cart</li>
          <button className="login" onClick={(e) => changeLogin(e)}>
            {loggedIn}
          </button>
        </ul>
      </div>
    </div>
  );
};

export default Header;
