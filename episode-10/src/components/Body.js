import RestaurantCard from "./RestaurantCard";
import { useEffect, useState } from "react";
import Shimmer from "./Shimmer";
import { Link } from "react-router-dom";
import useOnlineStatus from "../utils/useOnlineStatus";
// import { RESTAURANT_LIST_API } from "../utils/constants";
import useRestaurant from "../utils/useRestaurant";

const Body = () => {
  // const [listOfRestaurant, setListofRestaurant] = useState([]);
  const [searchInput, setSearchInput] = useState("");
  // const [backupRestaurant, setBackupRestaurant] = useState([]);
  const { listOfRestaurant, setListofRestaurant, backupRestaurant } =
    useRestaurant();

  // useEffect(() => {
  //   fetchData();
  // }, []);

  // async function fetchData() {
  //   const data = await fetch(RESTAURANT_LIST_API);
  //   const jsonData = await data.json();
  //   setListofRestaurant(
  //     jsonData?.data?.cards[1]?.card?.card?.gridElements?.infoWithStyle
  //       ?.restaurants
  //   );
  //   setBackupRestaurant(
  //     jsonData?.data?.cards[1]?.card?.card?.gridElements?.infoWithStyle
  //       ?.restaurants
  //   );
  // }

  function filteredRestaurant(e) {
    const filterEntries = listOfRestaurant.filter((restaurant) => {
      return restaurant.info.avgRating > 4;
    });
    setListofRestaurant(filterEntries);
  }

  function inputCapture(e) {
    setSearchInput(e.target.value);
  }

  function filterSearch(e) {
    let copiedRestaurant = [...backupRestaurant];
    let result = copiedRestaurant.filter((restaurant) => {
      return restaurant.info.name
        .toLowerCase()
        .includes(searchInput.toLowerCase());
    });
    setListofRestaurant(result);
  }

  const onlineStatus = useOnlineStatus();

  if (onlineStatus === false) {
    return (
      <div>
        <h1>
          Look like you're offline!! please check your internet connection and
          try again
        </h1>
      </div>
    );
  }

  return listOfRestaurant?.length === 0 ? (
    <Shimmer />
  ) : (
    <div className="body">
      <div className="filter flex">
        <div className="search m-4 p-4">
          <input
            type="text"
            className="border border-solid border-black"
            value={searchInput}
            onChange={(e) => inputCapture(e)}
          />
          <button
            className="px-4 py-2 bg-green-100 m-4 rounded-lg"
            onClick={(e) => filterSearch(e)}
          >
            Search
          </button>
        </div>
        <div className="m-4 p-4">
          <button
            className="px-2 py-2 bg-gray-100 m-4 rounded-lg"
            onClick={(e) => filteredRestaurant(e)}
          >
            Top Rated Restaurants
          </button>
        </div>
      </div>
      <div className="flex flex-wrap">
        {listOfRestaurant?.map((resObj) => {
          // console.log(resObj);
          return (
            <Link to={"restaurants/" + resObj.info.id} key={resObj.info.id}>
              <RestaurantCard resData={resObj} />
            </Link>
          );
        })}
      </div>
    </div>
  );
};

export default Body;
