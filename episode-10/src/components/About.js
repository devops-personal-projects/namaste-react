import User from "./User";
import UserClass from "./UserClass";
import React from "react";

class About extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {}

  render() {
    return (
      <div>
        <h1>About Class Component</h1>
        <h2>This is namaste react</h2>
        {/* <UserClass name="First Component(class)" location="Chandrapur(class)" /> */}

        <User name="First Component(function)" />
      </div>
    );
  }
}

export default About;
