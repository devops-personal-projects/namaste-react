import { useState } from "react";
import { CDN_URL } from "../utils/constants";
import { Link } from "react-router-dom";

const Header = () => {
  const [loggedIn, setLoggedIn] = useState("Login");

  function changeLogin(e) {
    loggedIn === "Login" ? setLoggedIn("Logout") : setLoggedIn("Login");
  }

  return (
    <div className="header">
      <div className="logo-container">
        <img src={CDN_URL} className="logo" />
      </div>
      <div className="nav-items">
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/about">About Us</Link>
          </li>
          <li>
            <Link to="/contact">Contact Us</Link>
          </li>
          <li>Cart</li>
          <button className="login" onClick={(e) => changeLogin(e)}>
            {loggedIn}
          </button>
        </ul>
      </div>
    </div>
  );
};

export default Header;
