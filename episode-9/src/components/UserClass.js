import React from "react";

class UserClass extends React.Component {
  constructor(props) {
    super(props);
    // console.log(props);

    this.state = {
      userData: {
        name: "Dummy",
        location: "Default",
        avatarUrl: "http://dummy",
      },
    };
  }

  async componentDidMount() {
    const data = await fetch("https://api.github.com/users/akshaymarch7");
    const json = await data.json();
    console.log(json);
    this.setState({
      userData: json,
    });
  }

  render() {
    // const { name, location } = this.props;
    const { name, location, avatar_url } = this.state.userData;
    return (
      <div className="user-card">
        <img src={avatar_url} />
        <h2>Name: {name}</h2>
        <h3>Location: {location}</h3>
        <h4>Contact: @sanket83</h4>
      </div>
    );
  }
}

export default UserClass;
