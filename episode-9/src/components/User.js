import { useEffect, useState } from "react";

const User = ({ name }) => {
  const [count, setCount] = useState(0);

  useEffect(() => {
    console.log("useEffect");

    return () => {
      console.log("Calling the return from inside the useEffect return part");
    };
  }, []);

  return (
    <div className="user-card">
      <h1>Count: {count}</h1>
      <h2>Name: {name}</h2>
      <h3>Location: Chandrapur</h3>
      <h4>Contact: @sanket83</h4>
    </div>
  );
};

export default User;
