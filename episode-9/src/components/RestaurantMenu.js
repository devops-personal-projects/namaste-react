import Shimmer from "./Shimmer";
import { useParams } from "react-router-dom";
import useRestaurantMenu from "../utils/useRestaurantMenu";

const RestaurantMenu = () => {
  const { resId } = useParams();

  const resInfo = useRestaurantMenu(resId);

  if (resInfo === null) {
    return <Shimmer />;
  }

  const { text } = resInfo?.cards[0]?.card?.card;
  const { cuisines, costForTwoMessage } = resInfo?.cards[2]?.card?.card?.info;
  const { itemCards } =
    resInfo?.cards[4]?.groupedCard?.cardGroupMap?.REGULAR.cards[3]?.card?.card;
  // console.log(itemCards);

  // console.log(resInfo?.cards[4]?.groupedCard?.cardGroupMap?.REGULAR?.cards);

  // console.log(text);
  // console.log(resInfo?.cards[0]?.card?.card);

  return (
    <div className="menu">
      <h1>{text}</h1>
      <p>
        {cuisines.join(", ")} - {costForTwoMessage}
      </p>
      <ul>
        {itemCards.map((restaurant) => {
          // console.log(restaurant);
          return (
            <li key={restaurant?.card?.info?.id}>
              {restaurant?.card?.info?.name} -{" "}
              {restaurant?.card?.info?.price / 100}₹
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default RestaurantMenu;
