import RestaurantCard from "./RestaurantCard";
import { useEffect, useState } from "react";
import Shimmer from "./Shimmer";
import { Link } from "react-router-dom";
import useOnlineStatus from "../utils/useOnlineStatus";
// import { RESTAURANT_LIST_API } from "../utils/constants";
import useRestaurant from "../utils/useRestaurant";

const Body = () => {
  // const [listOfRestaurant, setListofRestaurant] = useState([]);
  const [searchInput, setSearchInput] = useState("");
  // const [backupRestaurant, setBackupRestaurant] = useState([]);
  const { listOfRestaurant, setListofRestaurant, backupRestaurant } =
    useRestaurant();

  // useEffect(() => {
  //   fetchData();
  // }, []);

  // async function fetchData() {
  //   const data = await fetch(RESTAURANT_LIST_API);
  //   const jsonData = await data.json();
  //   setListofRestaurant(
  //     jsonData?.data?.cards[1]?.card?.card?.gridElements?.infoWithStyle
  //       ?.restaurants
  //   );
  //   setBackupRestaurant(
  //     jsonData?.data?.cards[1]?.card?.card?.gridElements?.infoWithStyle
  //       ?.restaurants
  //   );
  // }

  function filteredRestaurant(e) {
    const filterEntries = listOfRestaurant.filter((restaurant) => {
      return restaurant.info.avgRating > 4;
    });
    setListofRestaurant(filterEntries);
  }

  function inputCapture(e) {
    setSearchInput(e.target.value);
  }

  function filterSearch(e) {
    let copiedRestaurant = [...backupRestaurant];
    let result = copiedRestaurant.filter((restaurant) => {
      return restaurant.info.name
        .toLowerCase()
        .includes(searchInput.toLowerCase());
    });
    setListofRestaurant(result);
  }

  const onlineStatus = useOnlineStatus();

  if (onlineStatus === false) {
    return (
      <div>
        <h1>
          Look like you're offline!! please check your internet connection and
          try again
        </h1>
      </div>
    );
  }

  return listOfRestaurant?.length === 0 ? (
    <Shimmer />
  ) : (
    <div className="body">
      <div className="filter">
        <div className="search">
          <input
            type="text"
            className="search-box"
            value={searchInput}
            onChange={(e) => inputCapture(e)}
          />
          <button onClick={(e) => filterSearch(e)}>Search</button>
        </div>
        <button className="filter-btn" onClick={(e) => filteredRestaurant(e)}>
          Top Rated Restaurants
        </button>
      </div>
      <div className="res-container">
        {listOfRestaurant?.map((resObj) => {
          // console.log(resObj);
          return (
            <Link to={"restaurants/" + resObj.info.id} key={resObj.info.id}>
              <RestaurantCard resData={resObj} />
            </Link>
          );
        })}
      </div>
    </div>
  );
};

export default Body;
