import { useState } from "react";
import { CDN_URL } from "../utils/constants";

const Header = () => {
  const [loggedIn, setLoggedIn] = useState("Login");
  console.log("Called the header component");

  function changeLogin(e) {
    loggedIn === "Login" ? setLoggedIn("Logout") : setLoggedIn("Login");
  }

  return (
    <div className="header">
      <div className="logo-container">
        <img src={CDN_URL} className="logo" />
      </div>
      <div className="nav-items">
        <ul>
          <li>Home</li>
          <li>About Us</li>
          <li>Contact Us</li>
          <li>Cart</li>
          <button className="login" onClick={(e) => changeLogin(e)}>
            {loggedIn}
          </button>
        </ul>
      </div>
    </div>
  );
};

export default Header;
