import RestaurantCard from "./RestaurantCard";
import { useEffect, useState } from "react";
import Shimmer from "./Shimmer";

const Body = () => {
  const [listOfRestaurant, setListofRestaurant] = useState([]);
  const [searchInput, setSearchInput] = useState("");
  const [backupRestaurant, setBackupRestaurant] = useState([]);

  useEffect(() => {
    fetchData();
  }, []);

  async function fetchData() {
    const data = await fetch(
      "https://www.swiggy.com/dapi/restaurants/list/v5?lat=19.9615398&lng=79.2961468&is-seo-homepage-enabled=true&page_type=DESKTOP_WEB_LISTING"
    );
    const jsonData = await data.json();
    setListofRestaurant(
      jsonData?.data?.cards[1]?.card?.card?.gridElements?.infoWithStyle
        ?.restaurants
    );
    setBackupRestaurant(
      jsonData?.data?.cards[1]?.card?.card?.gridElements?.infoWithStyle
        ?.restaurants
    );
  }

  function filteredRestaurant(e) {
    const filterEntries = listOfRestaurant.filter((restaurant) => {
      return restaurant.info.avgRating > 4;
    });
    setListofRestaurant(filterEntries);
  }

  function inputCapture(e) {
    setSearchInput(e.target.value);
  }

  function filterSearch(e) {
    let copiedRestaurant = [...backupRestaurant];
    let result = copiedRestaurant.filter((restaurant) => {
      return restaurant.info.name
        .toLowerCase()
        .includes(searchInput.toLowerCase());
    });
    setListofRestaurant(result);
  }

  return listOfRestaurant.length === 0 ? (
    <Shimmer />
  ) : (
    <div className="body">
      <div className="filter">
        <div className="search">
          <input
            type="text"
            className="search-box"
            value={searchInput}
            onChange={(e) => inputCapture(e)}
          />
          <button onClick={(e) => filterSearch(e)}>Search</button>
        </div>
        <button className="filter-btn" onClick={(e) => filteredRestaurant(e)}>
          Top Rated Restaurants
        </button>
      </div>
      <div className="res-container">
        {listOfRestaurant.map((resObj) => {
          return <RestaurantCard key={resObj.info.id} resData={resObj} />;
        })}
      </div>
    </div>
  );
};

export default Body;
